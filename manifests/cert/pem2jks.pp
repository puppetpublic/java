#
# Create a Java keystore file given a PEM certificate and its
# associated key file.  Include a CA certificate in the keystore if
# desired.  This initial implementation stores a single cert/key
# pair in the key store.
#
# Syntax:
#
#     java::cert::pem2jks { "<certname>":
#         ensure       => present,
#         walletKey    => "idg-<certname>-ssl-key",
#         walletPass   => "idg-<certname>-ssl-pwd",
#         pwdRequired  => false,
#         caCertFile   => "<cacertname>",
#         keystoreFile => "/etc/ssl/keystore/<certname>.jks",
#         owner        => "root",
#         group        => $operatingsystem ? {
#                             debian => "ssl-cert",
#                             ubuntu => "ssl-cert",
#                             redhat => "root",
#                         },
#     }
#
# The parameters walletKey, walletPass, and keystoreFile need to be
# specified only if the wallet objects do not follow the naming
# convention.  Certs are moved into the /etc/ssl/certs directory, 
# keys into the /etc/ssl/private directory, and keystores into
# the /etc/ssl/keystore directory.
#
# Example with just a cert and key in the keystore:
#
#    java::cert::pem2jks {'sunetid': ensure => present }
#
# Example that includes a CA Cert:
#
#    java::cert::pem2jks {'sunetid':
#        ensure     => present,
#        caCertFile => 'Stanford_Registry_CA_2003.pem'
#    }
#
# Example with cert, key, CA cert, and password protected key:
#
#    java::cert::pem2jks {'sunetid':
#        ensure => present,
#        caCertFile => 'Stanford_Registry_CA_2003.pem',
#        pwdRequired => true,
#    }

define java::cert::pem2jks(
    $ensure,
    $walletKey    = "NONE",
    $walletPwd    = "NONE",
    $pwdRequired  = false,
    $caCertFile   = "NONE",
    $keystoreFile = "NONE",
    $owner        = "root",
    $group        = "NONE"
) {
    case $ensure {
        "present", "absent": { }
        default: { crit "Invalid ensure value: ${ensure}" }
    }

    case $walletKey {
        "NONE":  { $key = "idg-${name}-ssl-key" }
        default: { $key = $walletKey }
    }
    case $walletPwd {
        "NONE":  { $pwd = "idg-${name}-ssl-pwd" }
        default: { $pwd = $walletPwd }
    }
    case $caCertFile {
        'NONE':  { $cacert = '' }
        default: { $cacert = $caCertFile }
    }
    case $keystoreFile {
        "NONE":  { $jksFile = "/etc/ssl/keystore/${name}.jks" }
        default: { $jksFile = $keystoreFile }
    }
    case $group {
        'NONE':  { $grp = $::operatingsystem ? {
                'debian' => 'ssl-cert',
                'ubuntu' => 'ssl-cert',
                'redhat' => 'root',
            }
        }
        default: { $grp = $group }
    }

    # Include the required java-tools package and make sure the
    # /etc/ssl/keystore directory exits.
    include apache::cert::packages,
            java::cert::keystore_dir,
            packages::stanford_java_tools

    # Install the cerificate
    $certFile = "/etc/ssl/certs/${name}.pem"
    file { $certFile:
        source  => "puppet:///modules/cert-files/${name}.pem",
        owner   => $owner,
        group   => $grp,
        mode    => 0644,
        notify  => Exec["rm keystore ${name}"],
    }
    apache::cert::hash { "${name}.pem": ensure => present }
    
    # Install the private key.
    $walletKeyFile = "/etc/ssl/private/${name}.key"
    base::wallet { $key:
        ensure  => $ensure,
        type    => "file",
        path    => $walletKeyFile,
        owner   => $owner,
        group   => $grp,
        mode    => 0640,
        notify  => Exec["rm keystore ${name}"],
    }

    # Install the password for the private key if we need one.
    if $pwdRequired {
        base::wallet { $pwd:
            ensure => $ensure,
            type   => "file",
            path   => "/etc/ssl/private/${name}.pwd",
            owner  => $owner,
            group  => $grp,
            mode   => 0640,
            notify => Exec["rm keystore ${name}"],
        }
    } else {
        base::wallet { $pwd:
            ensure => absent,
            path   => "/etc/ssl/private/${name}.pwd",
        }
    }

    # Create the keystore.  This command will execute until the keystore
    # file is successfully generated.
    exec { "pem2jks ${name}":
        command => "/usr/bin/pem2jks ${certFile} ${walletKeyFile} ${jksFile} ${cacert}",
        unless  => "/usr/bin/test -e ${jksFile}",
        require => [ Package['stanford-java-tools'],
                     File['/etc/ssl/keystore'],
                     File["$certFile"],
                     Base::Wallet[$key],
                     Base::Wallet[$pwd] ]
    }

    # When the supporting files are changed them will notify this
    # command to remore the keystore file.  This will force the
    # keystore to be recreated.
    exec { "rm keystore ${name}":
        command     => "rm -f ${jksFile}",
        refreshonly => true,
        require     => Package['stanford-java-tools'],
        notify      => Exec["pem2jks ${name}"],
    }
}
