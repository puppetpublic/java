# Make sure the keystore directory exists

class java::cert::keystore_dir {
    file { '/etc/ssl/keystore':
        mode   => 710,
        owner  => 'root',
        group  => 'ssl-cert',
        ensure => directory,
    }
}
