#
# Create a pcks12 file given a PEM certificate and its associated key
# file.
#
# Syntax:
#
#     java::cert::pem2pcks12 { "<certname>":
#         ensure       => present,
#         walletKey    => "idg-<certname>-ssl-key",
#         walletPass   => "idg-<certname>-ssl-pwd",
#         pwdRequired  => false,
#         owner        => "root",
#         group        => $operatingsystem ? {
#                             debian => "ssl-cert",
#                             ubuntu => "ssl-cert",
#                             redhat => "root",
#                         },
#     }
#
# The parameters walletKey, walletPass, and keystoreFile need to be
# specified only if the wallet objects do not follow the naming
# convention.  Certs are moved into the /etc/ssl/certs directory, 
# keys into the /etc/ssl/private directory, and the p12 file into
# the /etc/ssl/keystore directory.
#
# Example with just a cert and key in the keystore:
#
#    java::cert::pem2pkcs12 {'account-events-test': ensure => present }
#

define java::cert::pem2pkcs12(
    $ensure,
    $walletKey    = "NONE",
    $walletPwd    = "NONE",
    $pwdRequired  = false,
    $p12File      = "NONE",
    $owner        = "root",
    $group        = "NONE"
) {
    case $ensure {
        "present", "absent": { }
        default: { crit "Invalid ensure value: ${ensure}" }
    }

    case $walletKey {
        "NONE":  { $key = "idg-${name}-ssl-key" }
        default: { $key = $walletKey }
    }
    case $walletPwd {
        "NONE":  { $pwd = "idg-${name}-ssl-pwd" }
        default: { $pwd = $walletPwd }
    }
    case $p12File {
        "NONE":  { $pkcs12File = "/etc/ssl/keystore/${name}.p12" }
        default: { $pkcs12File = $p12File }
    }
    case $group {
        'NONE':  { $grp = $::operatingsystem ? {
                'debian' => 'ssl-cert',
                'ubuntu' => 'ssl-cert',
                'redhat' => 'root',
            }
        }
        default: { $grp = $group }
    }

    # Include the required java-tools package and make sure the
    # /etc/ssl/keystore directory exits.
    include java::cert::keystore_dir,
            packages::stanford_java_tools
            
    # Install the cerificate
    $certFile = "/etc/ssl/certs/${name}.pem"
    file { $certFile:
        source  => "puppet:///modules/cert-files/${name}.pem",
        owner   => $owner,
        group   => $grp,
        mode    => 0644,
        notify  => Exec["rm pkcs12 $name"],
    }

    # Install the private key.
    $walletKeyFile = "/etc/ssl/private/${name}.key"
    base::wallet { $key:
        ensure  => $ensure,
        type    => "file",
        path    => $walletKeyFile,
        owner   => $owner,
        group   => $grp,
        mode    => 0640,
        notify  => Exec["rm pkcs12 ${name}"],
    }

    # Install the password for the private key if we need one.
    if $pwdRequired {
        base::wallet { $pwd:
            ensure => $ensure,
            type   => "file",
            path   => "/etc/ssl/private/${name}.pwd",
            owner  => $owner,
            group  => $grp,
            mode   => 0640,
            notify => Exec["rm pkcs12 ${name}"],
        }
    } else {
        base::wallet { $pwd:
            ensure => absent,
            path   => "/etc/ssl/private/${name}.pwd",
        }
    }

    # Create the keystore.  This command will execute until the keystore
    # file is successfully generated.
    exec { "pem2pkcs12 ${name}":
        command => "/usr/bin/pem2pkcs12 ${certFile} ${walletKeyFile} ${pkcs12File}",
        unless  => "/usr/bin/test -e ${pkcs12File}",
        require => [ Package['stanford-java-tools'],
                     File['/etc/ssl/keystore'],
                     File[$certFile],
                     Base::Wallet[$key],
                     Base::Wallet[$pwd] ]
    }

    # When the supporting files are changed them will notify this
    # command to remore the keystore file.  This will force the
    # keystore to be recreated.
    exec { "rm pkcs12 ${name}":
        command     => "rm -f ${pkcs12File}",
        refreshonly => true,
        require     => Package['stanford-java-tools'],
        notify      => Exec["pem2pkcs12 $name"],
    }
}
