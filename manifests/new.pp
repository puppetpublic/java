# Parameterized class for all java versions
# must be invoked by "class {'java::new': javaversion => x}
# cannot be included since we don't have a default since it
# seems better to require explicitly choosing java version.

class java::new ($javaversion, $vendor='sun') {
  case $::operatingsystem {
    'redhat': {
      case $::lsbmajdistrelease {
        '4','5','6': {
          package { 
            "java-1.${javaversion}.0-${vendor}":
              ensure => present,
              alias  => "java${javaversion}";
            "java-1.${javaversion}.0-${vendor}-devel": ensure => present;
          }
        }
        default: { fail("Unknown RHEL version '${::lsbmajdistrelease}'") }    
      }
    }
    'debian', 'ubuntu': {
      $jpackage = $vendor ? {
        'sun'     => "sun-java${javaversion}-jdk",
        'openjdk' => "openjdk-${javaversion}-jdk"
      }
      package { $jpackage:
        ensure => installed,
        alias  => "java${javaversion}",
      }
    }
  }
}
