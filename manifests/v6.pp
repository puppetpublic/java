#
# Class for handling java6 installation
# TODO: Move to the new packages module area

class java::v6 {
    case $::operatingsystem {
        'redhat': {
            if ($::lsbmajdistrelease < 5) {
                fail "java::v6 not supported on this RHEL release"
            }
            package { 
                'java-1.6.0-sun':       ensure => present;
                'java-1.6.0-sun-devel': ensure => present;
            }
        }
        'debian': {
            if $::lsbdistcodename == 'wheezy' {
                package { 'openjdk-6-jdk':
                    ensure => present,
                    alias  => 'java6',
                }
            } else {
                preseed_debconf { 'preseed-sun-java6':
                    answer => 'sun-java6-bin.+shared/accepted-sun-dlj-v1-1.+true',
                    source => 'puppet:///modules/java/preseed-sun-java6';
                }

                package { 'sun-java6-jdk':
                    ensure  => present,
                    require => Preseed_debconf['preseed-sun-java6'],
                    alias   => 'java6',
                }
            }
        }
         'ubuntu': {

            package { 'openjdk-6-jdk':
                ensure  => present,
                alias   => 'java6',
            }
        }


    }
}
