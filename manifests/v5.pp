#
# Class for handling java5 installation
# TODO: move to the new packages module area

class java::v5 {
    case $::operatingsystem {
        'redhat': {
            if ($::lsbmajdistrelease < 5) {
                fail "java::v5 not supported on this RHEL release"
            }
            package { 
                'java-1.5.0-sun':       ensure => present;
                'java-1.5.0-sun-devel': ensure => present;
            }
        }
        'debian', 'ubuntu': {
            preseed_debconf { 'preseed-sun-java5':
                answer => 'sun-java5-bin.+shared/accepted-sun-dlj-v1-1.+true',
                source => 'puppet:///modules/java/preseed-sun-java5';
            }
            package { 'sun-java5-jdk':
                ensure  => present,
                require => Preseed_debconf['preseed-sun-java5'],
                alias   => 'java5',
            }
        }
    }
}
